﻿using System;
namespace Tetris
{
    public abstract class Block
    {
        protected int numericValue = 0;

        public abstract Boolean Fall(ref int[,] gameBoard, ref int currentBrickY, ref Boolean boardStateChanged);

        public abstract void MoveLeft(ref int[,] gameBoard, ref int currentBrickX, ref int currentBrickY, ref Boolean boardStateChanged);

        public abstract void MoveRight(ref int[,] gameBoard, ref int currentBrickX, ref int currentBrickY, ref Boolean boardStateChanged);
    }
}

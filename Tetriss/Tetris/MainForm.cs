﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace Tetris
{

	// ile ma miec cala "plansza" ktora widzi gracz?
	// widze ze narysowane w oknie jest 10/15 
	// trzeba dodac "straznikow" na dole, po lewej i po prawej 
	// zeby sie nie dalo przesunac klocka w lewo i w prawo poza plansze oraz zeby wykryc ze klocek dolecial juz do "ziemi" i dalej go nie przesowamy
	// oprocz straznikow trzeba dodac jakas przestrzen na gorze na "wygenerowanie" nowego klocka, pytanie ile "kwadratow" wysokosci bedzie mial najwiekszy klocek? 

	//sprobujmy namalowac wszystkie klocki w tetris tutaj w komentarzach "*" jako "zamalowane" pole oraz "#" jako "zamalowane" pole ale rowniez os obrotu:


	//  normalnie | zgodnie z zegarem  | do gory nogami | przeciwnie do zegara

	// "trojka z dziubkiem"
	//    *       |     *     |          |    *
	//   *#*      |     #*    |    *#*   |   *#
	//            |     *     |     *    |    *
	//
	// "kwadrat"
	//   ***      |    ***    |    ***   |   ***
	//   *#*      |    *#*    |    *#*   |   *#*
	//   ***      |    ***    |    ***   |   ***
	//
	//
	// "dlugi naroznik lewy"
	//     *      |     *     |          |   **
	//   *#*      |     #     |    *#*   |    #
	//            |     **    |    *     |    *
	//
	// "dlugi naroznik prawy"
	//            |     *     |    *     |    **
	//   *#*      |     #     |    *#*   |    #
	//     *      |    **     |          |    *
	//

	// uzupelnij wg swojego uznania jakie beda klocki :)
	//
	//"schodek lewy" 
	//          |      *     |         |    *
	// **#      |     **     |    ***  |   #*
	//  ***     |     *#     |   **#   |   ** 
	//          |     *      |         |   * 
	//
	//"schodek prawy"
	//
	//          |     *      |          |    *
	//   #**    |     *#     |  ***     |    **
	//  ***     |     **     |   #**    |    #*
	//          |      *     |          |     *
	//
	//
	// 
	// "piątka" 
	//
	//            |    *      |            |    *
	//            |    *      |            |    *
	//   **#**    |    #      |   **#**    |    #
	//            |    *      |            |    *
	//            |    *      |            |    *
	// 
	// "jedynka"
	//            |           |            |    
	//     #      |     #     |     #      |    #
	//            |           |            |     
	//
	// "podkowa"
	//            |    **      |           |    ** 
	//     * *    |    #       |   *#*     |     #
	//     *#*    |    **      |   * *     |    **

	// jak zaplanujemy wszystkie klocki to zobaczymy ktory z nich jest najwyzszy i tyle na razie potrzebujemy :) zeby dodac wlasnie tyle dodatkowych "kwadratow" na gorze



	// klocek (ten ktory spada generalnie, bedzie ich kilka typow ale kazdy z nich ogolnie to jest klocek ktory spada) 
	// -> "block"
	//
	// "kwadracik" (z kilku kwadracikow skladaja sie klocki, np klocek "trojka" to 3 kwadraciki) 
	// -> "brick"
	//
	// plansza 
	// -> gameBoard

	public partial class MainForm : Form
	{
		Pen blackPen = new Pen(Color.Black,3);
        int currentBrickX = 5;
        int currentBrickY = 0;

        Block currentBlock;

        Boolean boardStateChanged = false;

        static int gameBoardWidthInBricks = 12;
        static int gameBoardHeightInBricks = 20;
        static int brickSizeInPixels = 40;
        static int guardsValue = 1000;
        // nie wiem jak moge krocej nazwac te 4 zmnienne nizej
        static int pixelsFromLeftEndOfScreenToRightEndOfGamePlace = 650;
        static int pixelsFromLeftEndOfScreenToLeftEndOFGamePlace = 260;
        static int pixelsFromTopOfScreenToBottomOfGamePlace = 700;
        static int pixelsFromTopOfScreenToTopOfGamePlace = 50;
        
        int clearedLineIndex;
        
        int score = 0;
        


        int[,] gameBoard = new int[gameBoardWidthInBricks, gameBoardHeightInBricks];

		public MainForm()
		{
			currentBlock = new SimpleBlock(5, 0, ref gameBoard, ref currentBrickX);
			InitializeComponent();
			CreateGuards();
		}
		
		public void DrawGamePlace(PaintEventArgs e)
		{
            int drawGamePlaceX1 = 300;
            int drawGamePlaceY1 = 50;
            int drawGamePlaceX2 = 300;
            int drawGamePlaceY2 = 50;

			for(int i = 0; i < 11; i++)
			{
				e.Graphics.DrawLine(blackPen, drawGamePlaceX1, drawGamePlaceY1, drawGamePlaceX1, pixelsFromLeftEndOfScreenToRightEndOfGamePlace);
				drawGamePlaceX1 += brickSizeInPixels;
			}
			for(int j = 0; j < 16; j++)
			{
				e.Graphics.DrawLine(blackPen, drawGamePlaceX2, drawGamePlaceY2, pixelsFromTopOfScreenToBottomOfGamePlace, drawGamePlaceY2);
				drawGamePlaceY2 += brickSizeInPixels;
			}
		}

        public void CreateGuards()
		{	
			for(int i=0; i < gameBoardWidthInBricks; i++)
			{
				for(int j=0; j < gameBoardHeightInBricks; j++)
				{
					if(j == gameBoardHeightInBricks - 1)
					{
						gameBoard[i, j] = guardsValue;
					}
					if(i == 0 | i == gameBoardWidthInBricks - 1)
					{
						gameBoard[i, j] = guardsValue;
					}
				}
			}
		}
        
        public void MakeBlocksFallByOneAfterLineClear()
        {
        	int blockValue;
        	for(int b = clearedLineIndex; b >= 0; b --)
        	{
        		for(int a = 1; a < 11; a ++)
        		{
        			if(gameBoard[a, b] < guardsValue && gameBoard[a, b + 1] == 0)
        			{
        				blockValue = gameBoard[a, b];
        				gameBoard[a, b + 1] = blockValue;
        				gameBoard[a, b] = 0;
        			}
        		}
        	}
        }
		
		public void ClearLineWhenFull()
		{
			for(int i = 0; i < gameBoardHeightInBricks-1; i++)
			{
				if(gameBoard[1,i] > 100 && gameBoard[2,i] > 100 && gameBoard[3,i] > 100 &&
				   gameBoard[4,i] > 100 && gameBoard[5,i] > 100 && gameBoard[6,i] > 100 &&
				   gameBoard[7,i] > 100 && gameBoard[8,i] > 100 && gameBoard[9,i] > 100 &&
				   gameBoard[10,i] > 100)
				{
					for(int j = 1; j < gameBoardWidthInBricks-1; j++)
					{
						gameBoard[j,i] = 0;
					}
					clearedLineIndex = i;
					MakeBlocksFallByOneAfterLineClear();
					
					score += 100;
				}
			}
		}
		
		public void TimerIntervalScalingWithScore()
		{
			if (score > 100 && score < 500)
			{
				timer1.Interval = 500;
			}
			else if (score > 500 && score < 1000)
			{
				timer1.Interval = 300;
			}
			else if (score > 1000)
			{
				timer1.Interval = 200;
			}
		}
		public void DrawScore(PaintEventArgs e)
		{
			Font arial = new Font("Arial", 16);
			string scoreString = score.ToString();
			e.Graphics.DrawString(scoreString, arial, Brushes.Black, 100, 100);
		}
		
		public void ShowNewBlock()
		{
			score += 10;
            Random rnd = new Random();
            int choseBlock = rnd.Next(1,5);
            
            switch (choseBlock)
            {
           		case 1:
            	{
           			currentBlock = new SimpleBlock(5, 0, ref gameBoard, ref currentBrickX);
            		break;
           		}
           		case 2:
            	{
            		currentBlock = new SmallSquareBlock(5, 0, ref gameBoard, ref currentBrickX);
            		break;
            	}
            	case 3:
            	{
            		currentBlock = new TripleBrick(5, 0, ref gameBoard, ref currentBrickX);
            		break;
            	}
            	case 4:
            	{
            		currentBlock = new PlusBlock(5, 0, ref gameBoard, ref currentBrickX);
            		break;
            	}
            }
		}
		
		public void DrawBlock(PaintEventArgs e)
		{	
			for(int a = 0; a < gameBoardWidthInBricks; a++)
			{
				for(int b = 0; b < gameBoardHeightInBricks; b++)
				{
					
					if(gameBoard[a,b] == 1 && b > 3 || gameBoard[a,b] == 101 && b > 3)
						e.Graphics.FillRectangle(Brushes.Cyan, a * brickSizeInPixels + pixelsFromLeftEndOfScreenToLeftEndOFGamePlace, (b-4) * brickSizeInPixels + pixelsFromTopOfScreenToTopOfGamePlace, brickSizeInPixels, brickSizeInPixels);
					
					if(gameBoard[a,b] == 2 && b > 3 || gameBoard[a,b] == 102 && b > 3)
						e.Graphics.FillRectangle(Brushes.Yellow, a * brickSizeInPixels + pixelsFromLeftEndOfScreenToLeftEndOFGamePlace, (b-4) * brickSizeInPixels + pixelsFromTopOfScreenToTopOfGamePlace, brickSizeInPixels, brickSizeInPixels);
					
					if(gameBoard[a,b] == 3 && b > 3 || gameBoard[a,b] == 103 && b > 3)
						e.Graphics.FillRectangle(Brushes.Red, a * brickSizeInPixels + pixelsFromLeftEndOfScreenToLeftEndOFGamePlace, (b-4) * brickSizeInPixels + pixelsFromTopOfScreenToTopOfGamePlace, brickSizeInPixels, brickSizeInPixels);					
					
					if(gameBoard[a,b] == 4 && b > 3 || gameBoard[a,b] == 104 && b > 3)
						e.Graphics.FillRectangle(Brushes.Crimson, a * brickSizeInPixels + pixelsFromLeftEndOfScreenToLeftEndOFGamePlace, (b-4) * brickSizeInPixels + pixelsFromTopOfScreenToTopOfGamePlace, brickSizeInPixels, brickSizeInPixels);						

					
					
					
					if(gameBoard[a,b] !=0 && gameBoard[a,b] < guardsValue && b > 3)
						e.Graphics.DrawRectangle(blackPen, a * brickSizeInPixels + pixelsFromLeftEndOfScreenToLeftEndOFGamePlace, (b-4) * brickSizeInPixels + pixelsFromTopOfScreenToTopOfGamePlace, brickSizeInPixels, brickSizeInPixels);

				}
		 	}
		}
		
		
		void MainFormPaint(object sender, PaintEventArgs e)
		{
			DrawGamePlace(e);
			DrawBlock(e);
			DrawScore(e);
		}
		
		void Timer1Tick(object sender, EventArgs e)
		{
			TimerIntervalScalingWithScore();
			ClearLineWhenFull();
            // nazwe tej metody z "FallingJedynka" zmien na angielska ktora mowi co sie dzieje wewnarz a bedzie sie dzialo to co opisalem w niej, 
            //czyli samo opadanie ze zwrotna informacja czy cos sie ruszylo

            boardStateChanged = currentBlock.Fall(ref gameBoard, ref currentBrickY, ref boardStateChanged);

            //boardStateChanged = FallingBlock();

            if (!boardStateChanged)
            {
				ShowNewBlock();
            }
            // tutaj wiemy po wykonaniu powyzszej czy cos sie ruszylo, 
            // - jak sie ruszylo to fajnie, klocek jakis spada, nic z tym nie robimy
            // - jak sie nie ruszylo to znaczy ze wszystko co moglo to juz spadlo, powinnismy wygenerowac nowy klocek na samej gorze :)


            // zawsze, bez wzgledu na to czy cos sie ruszylo czy nie, sprawdzamy czy jest jakas pelna linia 
            //(samo sprawdzenie to skanowanie gameboarda wiersz po wierszu i sprawdzenie czy jest jakas komorka pusta "0" jak nie ma to znaczy ze linia jest pelna, powinnismy tez uwzglednic zeby straznikow "100" nie kasowac),
            // - jak jest to ja czyscimy (ustawiamy "0" w odpowiednich komorkach) i wywolujemy jeszcze raz metode ktora powoduje opadanie rzeczy ktore moga opasc czyli ta "FAllingjedynka" ale juz pewnie sie bedzie inaczej nazywala  
            // - jak nie ma pelnej linii to nic nie robimy
            // zeby byloa ladnie i zgodnie z najlepszymi praktykami programowania to samo sprawdzanie czy jest pelna linia i jej kasowanie zamknijmy w osobnej metodzie
            // ktora odda informacje zwrotna czy cos sie skasowalo, po czym mozna sprawdzic ta informacje i wywolac metode do spadania, logika bedzie czysta i czytelna
            
			Invalidate();
		}

        private void PrintGameBooardToLog()
        {
			for (int b = 0; b < gameBoardHeightInBricks; b++)
			{
                for (int a = 0; a < gameBoardWidthInBricks; a++)
                {
					Debug.Write("|");

					int val = gameBoard[a, b];
                    if (val >= 0 && val < 10)
                    {
                        Debug.Write("000");
                    }
                    else if (val >= 10 && val < guardsValue)
                    {
                        Debug.Write("0");
                    }
                    Debug.Write(gameBoard[a, b]);
                }
				Debug.WriteLine("|");
			}

			Debug.WriteLine("-------------------------------------------------------------");
			
			Debug.WriteLine("");
			Debug.Write("currentBlock:");
			Debug.WriteLine(currentBlock);
			Debug.Write("boardStateChanged:");
			Debug.WriteLine(boardStateChanged);
			Debug.Write("currentBrickX:");
			Debug.WriteLine(currentBrickX);
			Debug.Write("currentBrickY:");
			Debug.WriteLine(currentBrickY);
			Debug.WriteLine("");
			Debug.Write("Score:");
			Debug.WriteLine(score);
			Debug.Write("Timer Interval:");
			Debug.WriteLine(timer1.Interval);
			Debug.WriteLine("");
			
		}
        
        public void RestartGameBoard()
        {
        	for(int b = 0; b < gameBoardHeightInBricks; b++)
        	{
        		for(int a = 0; a < gameBoardWidthInBricks; a++)
        		{
        			if(gameBoard[a,b] < guardsValue)
        				gameBoard[a,b] = 0;
        		}
        	}
        	currentBrickY = 0;
        	currentBrickX = 5;
        }

        void MainFormKeyDown(object sender, KeyEventArgs e)
		{
        	if(e.KeyCode == Keys.Left)
        	{
        		currentBlock.MoveLeft(ref gameBoard, ref currentBrickX, ref currentBrickY, ref boardStateChanged);
        	}
        	
        	if(e.KeyCode == Keys.Right)
        	{
        		currentBlock.MoveRight(ref gameBoard, ref currentBrickX, ref currentBrickY, ref boardStateChanged);
        	}
        	
			if(e.KeyCode == Keys.P)
			{
				PrintGameBooardToLog();
				timer1.Enabled=false;
			}
			if(e.KeyCode == Keys.S)
			{
				timer1.Enabled=true;
			}
			if(e.KeyCode == Keys.R)
			{
				RestartGameBoard();
			}
		}
	}
}
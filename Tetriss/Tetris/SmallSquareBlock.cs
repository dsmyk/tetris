﻿using System;
using System.Collections;

namespace Tetris
{
	public class SmallSquareBlock : Block
	{
		private ArrayList bricks;

		public SmallSquareBlock(int x, int y, ref int[,] gameBoard, ref int currentBrickX)
		{
            numericValue = 2;
		    bricks = new ArrayList();

			// "mały kwadrat" 
			//            |           |            |    
			//   #*       |    #*     |    #*      |    #*
			//   **       |    **     |    **      |    **
			//
			Brick brick1 = new Brick()
			{
				X = x,
				Y = y
			};
			Brick brick2 = new Brick()
			{
				X = x+1,
				Y = y
			};
			Brick brick3 = new Brick()
			{
				X = x,
				Y = y+1
			};
			Brick brick4 = new Brick()
			{
				X = x+1,
				Y = y+1
			};
			bricks.Add(brick1);
			bricks.Add(brick2);
			bricks.Add(brick3);
			bricks.Add(brick4);

            foreach (Brick brick in bricks)
            {
                gameBoard[brick.X, brick.Y] = numericValue;
            }

            currentBrickX = 5;
		}

        public override bool Fall(ref int[,] gameBoard, ref int currentBrickY, ref bool boardStateChanged)
        {
            //zalozenie 1. nie zmienil sie board, jak sie zmieni to przestawimy na true
            boardStateChanged = false;

            //zalozenie 2. klocek jako calosc kilku kwadracikow moze opasc w dol, jak spotkamy przeszkode to przestawimy ze nie moze
            bool canFallDown = true;

            //teraz przelecimy sie po wszystkich kwadracikach tego bloczka i sprawdzimy czy ma pod soba przestrzen lub samego siebie
            foreach (Brick brick in bricks)
            {
                //sprawdzajac kazdy kwadracik tego klocka, jak pod spodem jest cos co nie jest zerem (czyli niepuste) 
                //i nie jest to czesc tego wlasnie kolcka to znaczy ze jest inny klocek "wtopiony" w boarda albo straznik
                // co oznacza ze nie moze dalej spadac bo ma przeszkode (caly klocek, wszystkie jego kwadraciki musza sie zatrzymac)
                if (gameBoard[brick.X, brick.Y + 1] != 0 && gameBoard[brick.X, brick.Y + 1] != numericValue)
                {
                    canFallDown = false;
                }
            }

            //jak moze spadac to opada o 1, jak nie moze spadac to go "wtapiamy"
            if (canFallDown)
            {
                //opada o jeden, stan boarda sie zmienil, 
                //zwroc uwage na to ze "opadanie" dzieje sie rzedami od lewej do prawej i od dolu do gory - zeby nie "zamazac" sobie opadania z poprzedniego wiersza
                //w sytuacji jak bysmy "opadali" najpierw gorny a pozniej dolny rzad to by nam wiersze "ginely" :)
                for (int i = bricks.Count - 1; i >= 0; i--)
                {
                    Brick b = (Tetris.Brick)bricks[i];
                    //wspolrzedne musimy zmienic w 2 miejscach: 
                    //1. na boardzie
                    gameBoard[b.X, b.Y + 1] = numericValue;
                    gameBoard[b.X, b.Y] = 0;
                    //2. u nas tutaj w wewnetrznej liscie klockow
                    b.Y = b.Y + 1;
                }
                boardStateChanged = true;
            }
            else
            {
                //wtapiamy, stan boarda sie nie zmienil
				for (int i = bricks.Count - 1; i >= 0; i--)
				{
					Brick b = (Tetris.Brick)bricks[i];
					gameBoard[b.X, b.Y] += 100;
				}
            }
            return boardStateChanged;

        }

		public override void MoveLeft(ref int[,] gameBoard, ref int currentBrickX, ref int currentBrickY, ref bool boardStateChanged)
		{		
  			boardStateChanged = false;
   			bool canMoveLeft = true;

			foreach (Brick brick in bricks)
			{
                if (gameBoard[brick.X - 1, brick.Y] != 0 && gameBoard[brick.X - 1, brick.Y] != numericValue)
				{
                    canMoveLeft = false;
				}
			}

			if (canMoveLeft)
			{
				for (int i = 0; i < bricks.Count; i++)
				{
                    //tutaj pamietamy ze przesuniecie w lewo -> zaczynamy od klockow z lewej strony, inaczej bysmy sobie nadpisywali
					Brick b = (Tetris.Brick)bricks[i];
					gameBoard[b.X - 1, b.Y] = numericValue;
					gameBoard[b.X, b.Y] = 0;
					b.X = b.X - 1;
				}
				boardStateChanged = true;
			}
		}
		public override void MoveRight(ref int[,] gameBoard, ref int currentBrickX, ref int currentBrickY, ref bool boardStateChanged)
		{
			//tutaj pamietamy ze przesuniecie w prawo -> zaczynamy od klockow z prawej strony zebysmy sobie nie nadpisywali (uwaga! inaczej niz przy przesunieciu w lewo
			
            boardStateChanged = false;
            bool canMoveRight = true;
            
            foreach (Brick brick in bricks)
            {
            	if (gameBoard[brick.X + 1, brick.Y] != 0 && gameBoard[brick.X + 1, brick.Y] != numericValue)
            	{
            		canMoveRight = false;
            	}
            }
            
            if (canMoveRight)
            {
            	for (int i = bricks.Count -1; i >= 0; i--)
            	{
            		Brick b = (Tetris.Brick)bricks[i];
            		gameBoard[b.X + 1, b.Y] = numericValue;
            		gameBoard[b.X, b.Y] = 0;
            		b.X = b.X + 1;
            	}
            	boardStateChanged = true;
            }
		}
	}
}

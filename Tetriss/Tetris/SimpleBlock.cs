﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace Tetris
{
    public class SimpleBlock : Block
    {	
        private ArrayList bricks;

        public SimpleBlock(int x, int y, ref int[,] gameBoard, ref int currentBrickX)
        {
        	numericValue = 1;
        	
            bricks = new ArrayList();

            Brick brick1 = new Brick()
            {
                X = x,
                Y = y
            };
            
            bricks.Add(brick1);
            
            foreach (Brick brick in bricks)
            {
            	gameBoard[brick.X, brick.Y] = numericValue;
            }
            
            currentBrickX = 5;
        }
 		public override bool Fall(ref int[,] gameBoard, ref int currentBrickY, ref Boolean boardStateChanged)
		{
			boardStateChanged = false;
			bool canFallDown = true;
			
			foreach (Brick brick in bricks)
			{
				if(gameBoard[brick.X,brick.Y + 1] != 0 && gameBoard[brick.X, brick.Y + 1] != numericValue)
				{
					canFallDown = false;
				}
			}
			
			if (canFallDown)
			{
				for (int i = bricks.Count - 1; i >= 0; i--)
				{
					Brick b = (Tetris.Brick)bricks[i];
					
					gameBoard[b.X, b.Y + 1] = numericValue;
					gameBoard[b.X, b.Y] = 0;
					
					b.Y = b.Y + 1;
				}
				boardStateChanged = true;
			}
			else
			{
				for(int i = bricks.Count - 1; i >= 0; i--)
				{
					Brick b = (Tetris.Brick)bricks[i];
					gameBoard[b.X, b.Y] += 100;
				}
			}
			return boardStateChanged;
		}

		 public override void MoveLeft(ref int[,] gameBoard, ref int currentBrickX, ref int currentBrickY, ref bool boardStateChanged)
		{		
  			boardStateChanged = false;
   			bool canMoveLeft = true;

			foreach (Brick brick in bricks)
			{
                if (gameBoard[brick.X - 1, brick.Y] != 0 && gameBoard[brick.X - 1, brick.Y] != numericValue)
				{
                    canMoveLeft = false;
				}
			}

			if (canMoveLeft)
			{
				for (int i = 0; i < bricks.Count; i++)
				{
					Brick b = (Tetris.Brick)bricks[i];
					
					gameBoard[b.X - 1, b.Y] = numericValue;
					gameBoard[b.X, b.Y] = 0;
					
					b.X = b.X - 1;
				}
				boardStateChanged = true;
			}
		 }
        
        public override void MoveRight(ref int[,] gameBoard, ref int currentBrickX, ref int currentBrickY, ref bool boardStateChanged)
		{
            boardStateChanged = false;
            bool canMoveRight = true;
            
            foreach (Brick brick in bricks)
            {
            	if (gameBoard[brick.X + 1, brick.Y] != 0 && gameBoard[brick.X + 1, brick.Y] != numericValue)
            	{
            		canMoveRight = false;
            	}
            }
            
            if (canMoveRight)
            {
            	for (int i = bricks.Count -1; i >= 0; i--)
            	{
            		Brick b = (Tetris.Brick)bricks[i];
            		
            		gameBoard[b.X + 1, b.Y] = numericValue;
            		gameBoard[b.X, b.Y] = 0;
            		
            		b.X = b.X + 1;
            	}
            	boardStateChanged = true;
            }
    	}
    }
}
